/// <reference types="cypress" />

context('Kiểm thử mẫu đăng ký của Herbalife bằng phương pháp hộp đen.', () => {
    beforeEach(() => {
        cy.visit('https://accounts.myherbalife.com/Account/Create')
    })

    describe('Kiểm thử ô nhập địa chỉ email và email xác nhận.', () => {

        it('Địa chỉ email không hợp lệ.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail').should('have.value', 'testFakeEmail')
            cy.get('#cEmail').click()
            cy.get('#email-error').contains('Please enter a valid email address.')
        })

        it('Địa chỉ email xác nhận không khớp.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail1@gmail.com').should('have.value', 'testFakeEmail1@gmail.com')
            cy.get('#show-pass').click()
            cy.get('#cemail-error').contains('Email addresses do not match')

        })

        it('Địa chỉ email đã được sử dụng.', () => {
            // Enter Email
            cy.get('#email')
                .type('fake@email.com').should('have.value', 'fake@email.com')
            cy.get('#show-pass').click()
            cy.get('#signin-email').contains('fake@email.com')

        })
    })

    describe('Kiểm thử ô nhập mật khẩu và xác nhận mật khẩu.', () => {
        it('Độ dài mật khẩu không hợp lệ.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Click Show Password
            cy.get('#show-pass').click()
            // Enter Password
            cy.get('#Password')
                .type('123').should('have.value', '123')
            cy.get('#password-help').contains('Your password must be 8 characters in length with at least one number and one symbol.')
        })


        it('Mật khẩu không chứa ký tự số.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Click Show Password
            cy.get('#show-pass').click()
            // Enter Password
            cy.get('#Password')
                .type('Pa$$word').should('have.value', 'Pa$$word')
            cy.get('#password-help').contains('Your password must be 8 characters in length with at least one number and one symbol.')
        })

        it('Mật khẩu không chứa ký tự đặc biệt.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Click Show Password
            cy.get('#show-pass').click()
            // Enter Password
            cy.get('#Password')
                .type('Pa11word').should('have.value', 'Pa11word')
            cy.get('#password-help').contains('Your password must be 8 characters in length with at least one number and one symbol.')
        })

        it('Mật khẩu và xác nhận mật khẩu không khớp.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Click Show Password
            cy.get('#show-pass').click()
            // Enter Password
            cy.get('#Password')
                .type('Pa$$wrd123').should('have.value', 'Pa$$wrd123')
            cy.get('#cpassword')
                .type('Pa$$wrd123').should('have.value', 'Pa$$wrd123')
            cy.get('#hide-pass').click()
            cy.get('#cpassword-error').contains('Passwords do not match.')
        })
    })

    describe('Kiểm thử tất cả các ô giá trị với giá trị hợp lệ.', () => {
        it('Tất cả các ô giá trị được nhập với giá trị hợp lệ.', () => {
            // Enter Email
            cy.get('#email')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Enter Confirm Email
            cy.get('#cEmail')
                .type('testFakeEmail@gmail.com').should('have.value', 'testFakeEmail@gmail.com')
            // Click Show Password
            cy.get('#show-pass').click()
            // Enter Password
            cy.get('#Password')
                .type('Pa$$wrd123').should('have.value', 'Pa$$wrd123')
            // Enter RePassword
            cy.get('#cpassword')
                .type('Pa$$wrd123').should('have.value', 'Pa$$wrd123')
            // // Click Register button
            // cy.get('#create-account').click()

        })
    })
})

